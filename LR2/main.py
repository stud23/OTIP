'''
Module for working with ElasticSearch throuhgt the console

'''

import argparse
import sys

import lab2

if __name__ == "__main__":
    elastic = lab2.Elastic('2018-3-4-vor', hosts=['172.17.0.1'])

    parser = argparse.ArgumentParser()

    parser.add_argument("act", choices=['create', 'add-book', 'add-books',
                                        'count-books-with-word', 'search-books',
                                        'search-dates', 'calc-date', 'top-words'])
    parser.add_argument("-a", "--author", dest="author", type=str, default=None, required=False)
    parser.add_argument("-y", "--year", dest="year", type=str, default=None, required=False)
    parser.add_argument("-n", "--name", dest="name", type=str, default=None, required=False)
    parser.add_argument("-f", "--from", dest="gte", type=str, default=None, required=False)
    parser.add_argument("-t", "--till", dest="lte", type=str, default=None, required=False)
    parser.add_argument("-d", "--date", dest="date", type=str, default=None, required=False)

    action = sys.argv[1]

    if action == 'create':
        print(elastic.create_index())

    elif action == 'add-book':
        parser.add_argument("book")
        args = parser.parse_args()

        if not args.year.isdigit():
            raise Exception(f"Wrong argument 'year={args.year}'") from None

        try:
            print(elastic.add_book(args.author, args.year, args.name, args.book), args.book)
        except FileNotFoundError:
            raise Exception(f"Wrong book path '{args.book}'") from None

    elif action == 'add-books':
        parser.add_argument("books")
        args = parser.parse_args()

        try:
            response = elastic.add_books(args.books)
        except FileNotFoundError:
            raise Exception(f"Wrong books path '{args.books}'") from None

        print('created:')
        for book in response:
            print(book)

    elif action == 'count-books-with-word':
        parser.add_argument("word")
        args = parser.parse_args()
        response = elastic.get_books_with_word(args.word)

        print(len(response))
        for book in response:
            print(book.name, ', ', book.author, ', ', book.year, sep='')

    elif action == 'search-books':
        parser.add_argument("phrase")
        args = parser.parse_args()
        response = elastic.get_books_with_author_and_phrase(args.author, args.phrase)

        print(len(response))
        for book in response:
            print(book.name, ', ', book.author, ', ', book.year, sep='')

    elif action == 'search-dates':
        parser.add_argument("phrase")
        args = parser.parse_args()
        response = elastic.get_books_with_years_range_and_no_phrase(args.gte, args.lte, args.phrase)

        print(len(response))
        for book in response:
            print(book.name, ', ', book.author, ', ', book.year, sep='')

    elif action == 'calc-date':
        args = parser.parse_args()
        response = elastic.get_average_of_book_years_for_author(args.author)

        print(response)

    elif action == 'top-words':
        args = parser.parse_args()
        if not args.date.isdigit():
            raise Exception(f"Wrong argument 'date={args.date}'") from None

        response = elastic.get_top10_words_for_year(args.date)
        if response:
            for word, ttf in response.items():
                print(word, ttf)
        else:
            print('No words')

    else:
        raise Exception(f"Wrong argument '{action}'") from None
