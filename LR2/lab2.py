'''
Class for working with ElasticSearch

Classes:

    Elastic

'''

import os

from elasticsearch_dsl import connections, Document, Mapping, Index, Q

class Elastic:
    '''
        Class for working with ElasticSearch

        Classes:

            BookDocument

        Methods:

            create_index
            create_doc
            add_book
            add_books
            get_books_with_word
            get_books_with_author_and_phrase
            get_average_of_book_years_for_author
            get_top10_words_for_year
    '''
    def __init__(self, index_name: str, hosts: list=['172.17.0.1']) -> None:
        self.es_connection = connections.create_connection(hosts=hosts)
        self.index_name = index_name

    def create_index(self) -> str:
        '''
        Creates index.

            Returns:
                report: Report on index creation report

        '''

        my_index = Index(self.index_name)

        my_index.settings(
            number_of_shards=1,
            number_of_replicas=0,
            analysis={"filter": {
                        "russian_stop": {
                            "type": "stop",
                            "stopwords": ["_russian_", "князь", "повезет", "сорок"]}},
                     "analyzer": {
                         "rus_tok":  {
                             "tokenizer": "standard",
                             "filter": ["lowercase", "russian_stop"]
                                     }
                                 }
                     }
        )

        my_mapping = Mapping() 
        text_field = {'type': 'text',
                     'analyzer': "rus_tok"}

        my_mapping.field('name', text_field)
        my_mapping.field('author', text_field)
        my_mapping.field('year', 'short')
        my_mapping.field('text', text_field)

        my_index.mapping(my_mapping)

        my_index.delete(ignore=404)
        return my_index.create()

    class BookDocument(Document):
        '''
        Class for documents
        '''

    def create_doc(self, **params: dict) -> str:
        '''
        Creates document.

            Parameters:
                params: Parameters for document

            Returns:
                report: Report on document creation

        '''
        doc = self.BookDocument(_index=self.index_name, **params)
        return doc.save()

    def add_book(self, author: str, year: int or str, name: str, path: str) -> str:
        """
        Adds book to index.

            Parameters:
                author: Book author
                year: Book year
                name: Book name
                parh: Path to book

            Returns:
                report: Report on document creation

        """
        with open(path) as file_name:
            text = file_name.read()
        return self.create_doc(author=author, year=year, name=name, text=text)

    def add_books(self, path: str) -> list:
        """
        Adds book to index.

            Parameters:
                parh: Path to books

            Returns:
                report: List of created documents
        """
        book_names = os.listdir(path)
        created_books = []

        for book in book_names:
            book_meta = [s.strip() for s in book.split('-')]

            if len(book_meta) >= 3 and book_meta[-1].isdigit():
                
                year = book_meta[-1]
                author = book_meta[-2]
                name = ''.join(book_meta[:-2])

                book_name = os.path.join(path, book)

                self.add_book(author=author, year=year, name=name, path=book_name)
                created_books.append(book_name)
        return created_books

    def get_books_with_word(self, word: str) -> dict:
        '''
        Shows books with word.

            Parameters:
                word: Search word

            Returns:
                dict: Found books

        '''
        search = self.BookDocument.search(index=self.index_name)

        search = search.query("match", text=word)
        response = search.execute()
        return response

    def get_books_with_author_and_phrase(self, author: str, phrase: str) -> dict:
        '''
        Shows books with a certain author and a certain phrase.

            Parameters:
                author: Search author
                phrase: Search phrase

            Returns:
                dict: Found books

        '''
        query = Q("match", author=author) & Q('match_phrase', text=phrase)

        search = self.BookDocument.search(index=self.index_name)
        search = search.query(query)
        response = search.execute()
        return response

    def get_books_with_years_range_and_no_phrase(self, first_year: int or str,
                                                 last_year: int or str, phrase: str) -> dict:
        '''
        Shows books without a phrase in the range of years.

            Parameters:
                first_year: The first year in range
                last_year: The last year in range
                phrase: Search phrase

            Returns:
                dict: Found books

        '''
        query = Q({"range": {"year": {"gte": first_year, "lte": last_year}}}) & \
               ~Q('match_phrase', text=phrase)

        search = self.BookDocument.search(index=self.index_name)
        search = search.query(query)
        response = search.execute()
        return response

    def get_average_of_book_years_for_author(self, author: str) -> int:
        '''
        Shows average of book years for a certain author.

            Parameters:
                author: Search author

            Returns:
                int: The average year

        '''
        query = Q("match", author=author)

        search = self.BookDocument.search(index=self.index_name)
        search = search.query(query)
        search.aggs.metric('average_year', 'avg', field='year')

        response = search.execute()
        return response.aggregations['average_year']['value']

    def get_top10_words_for_year(self, year: int or str) -> None or dict:
        '''
        Shows top 10 words for a certain author.

            Parameters:
                year: Search get

            Returns:
                None or dict: Words and their frequency

        '''
        query = Q("match", year=year)

        search = self.BookDocument.search(index=self.index_name)
        search = search.query(query)

        response = search.execute()
        ids = []
        for book in response:
            ids.append(book.meta.id)

        if len(ids) == 0:
            return None

        response = self.es_connection.mtermvectors(index=self.index_name,
                                   ids=ids,
                                   fields=['text'],
                                   field_statistics=False,
                                   positions=False,
                                   offsets=False,
                                   term_statistics=True)

        terms = {}

        for book in response["docs"]:
            for term, stat in book["term_vectors"]["text"]["terms"].items():
                if term not in terms:
                    terms[term] = stat['ttf']


        sorted_tuples = sorted(terms.items(), key=lambda item: item[1], reverse=True)
        top_sorted_tuples = sorted_tuples[:10]
        sorted_terms = dict(top_sorted_tuples)
        if len(sorted_terms) == 0:
            return None

        return sorted_terms
