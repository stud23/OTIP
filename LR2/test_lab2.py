'''
Module for testing class Elastic
'''
import time

from elasticsearch_dsl import Index

import lab2

elastic = lab2.Elastic('test_index')


def test_create_index():
    '''
    Tests the create_index function
    '''

    my_index = Index(elastic.index_name)
    if my_index.exists():
        my_index.delete()

    elastic.create_index()
    assert my_index.exists()
    mapping = my_index.get_mapping()[elastic.index_name]['mappings']['properties']
    assert mapping['year']['type'] == 'short'
    assert mapping['name']['type'] == 'text'
    assert mapping['author']['type'] == 'text'
    assert mapping['text']['type'] == 'text'

def test_create_doc():
    '''
    Tests the create_doc function
    '''

    doc_id = 'test_id'
    if elastic.BookDocument.exists(id=doc_id, index=elastic.index_name):
        elastic.BookDocument(_id=doc_id).delete()

    elastic.create_doc(_id=doc_id, author='Tolstoy', year=1865, name='ВойнаИМир',
                       text='test test test')

    assert elastic.BookDocument.exists(id=doc_id, index=elastic.index_name)

def test_get_books_with_word():
    '''
    Tests the get_books_with_word function
    '''

    time.sleep(1)
    test_word = "test"
    response = elastic.get_books_with_word(test_word)
    assert len(response) == 1
    assert response[0].name == "ВойнаИМир"
    assert response[0].author == "Tolstoy"
    assert response[0].year == 1865

def test_get_books_with_author_and_phrase():
    '''
    Tests the get_books_with_author_and_phrasefunction
    '''

    author = 'Tolstoy'
    phrase = 'test test test'
    response = elastic.get_books_with_author_and_phrase(author, phrase)

    assert len(response) == 1
    assert response[0].name == "ВойнаИМир"
    assert response[0].author == "Tolstoy"
    assert response[0].year == 1865

def test_get_books_with_years_range_and_no_phrase():
    '''
    Tests the get_books_with_years_range_and_no_phrase function
    '''

    first_year = 1000
    last_year = 2000
    phrase = 'марцепан'
    response = elastic.get_books_with_years_range_and_no_phrase(first_year, last_year, phrase)

    assert len(response) == 1
    assert response[0].name == "ВойнаИМир"
    assert response[0].author == "Tolstoy"
    assert response[0].year == 1865

def test_get_average_of_book_years_for_author():
    '''
    Tests the get_average_of_book_years_for_author function
    '''

    author = 'Tolstoy'
    response = elastic.get_average_of_book_years_for_author(author)

    assert response == 1865.0

def test_get_top10_words_for_year():
    '''
    Tests the get_top10_words_for_year( function
    '''

    year = '1865'
    response = elastic.get_top10_words_for_year(year)

    assert response == {'test': 3}

    year = 1860
    response = elastic.get_top10_words_for_year(year)

    assert response is None
