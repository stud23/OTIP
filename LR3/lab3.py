# pylint: disable=W0102,I1101

'''
Class for working with ElasticSearch

Classes:

    Elastic

'''

from string import punctuation
from heapq import nlargest

import roman
import spacy

from spacy.lang.ru.stop_words import STOP_WORDS
from elasticsearch_dsl import connections, Document, Mapping, Index, Q
from lxml import etree


class Elastic:
    '''
        Class for working with ElasticSearch

        Classes:

            BookDocument

        Methods:

            create_index
            create_doc
            add_book
            add_books
            get_books_with_word
            get_books_with_author_and_phrase
            get_average_of_book_years_for_author
            get_top10_words_for_year
    '''
    def __init__(self, index_name: str, hosts: list=['172.17.0.1']) -> None:
        self.es_connection = connections.create_connection(hosts=hosts)
        self.index_name = index_name
        self.stopwords = list(STOP_WORDS)
        self.punctuation = punctuation + '\n'


        self.nlp = spacy.load("ru_core_news_sm")

        self.word2number = {
            'ПЕРВАЯ': 1,
            'ВТОРАЯ': 2,
            'ТРЕТЬЯ': 3,
            'ЧЕТВЕРТАЯ': 4,
            'ПЯТАЯ': 5,
            'ШЕСТАЯ': 6,
            'СЕДЬМАЯ': 7,
            'ВОСЬМАЯ': 8,
            'ДЕВЯТАЯ': 9,
            'ДЕСЯТАЯ': 10
        }

    def create_index(self) -> str:
        '''
        Creates index.

            Returns:
                report: Index creation report

        '''

        my_index = Index(self.index_name)

        my_index.settings(
            number_of_shards=1,
            number_of_replicas=0,
            analysis={"filter": {
                        "russian_stop": {
                            "type": "stop",
                            "stopwords": ["_russian_"]}},
                     "analyzer": {
                         "rus_tok":  {
                             "tokenizer": "standard",
                             "filter": ["lowercase", "russian_stop"]
                                     }
                                 }
                     }
        )

        my_mapping = Mapping() #type = _doc
        text_field = {'type': 'text',
                     'analyzer': "rus_tok"}

        my_mapping.field('volume', text_field)
        my_mapping.field('part', text_field)
        my_mapping.field('chapter', text_field)
        my_mapping.field('name', text_field)
        my_mapping.field('author', text_field)
        my_mapping.field('text', text_field)

        my_index.mapping(my_mapping)

        my_index.delete(ignore=404)
        return my_index.create()

    class BookDocument(Document):
        '''
        Class for documents
        '''

    def create_doc(self, **params: dict) -> str:
        '''
        Creates document.

            Parameters:
                params: Parameters for document

            Returns:
                report: Report on document creation

        '''
        doc = self.BookDocument(_index=self.index_name, **params)
        return doc.save()

    def parse_book(self, book_path: str, name: str, author: str) -> str:
        '''
        Parses book and loads it into index.

            Parameters:
                book_path: Book path
                author: Book author
                name: Book name

            Returns:
                str: Report on the analyzed book

        '''

        root = etree.parse(book_path).getroot()

        extra_str = '{http://www.gribuser.ru/xml/fictionbook/2.1}'
        sections = []

        for section in root.getchildren()[1]:
            children = section.getchildren()
            if section.tag == f'{extra_str}section' and children[0].tag == extra_str + 'title':
                sections.append(section)

        volume = sections[0].getchildren()[0].getchildren()[0].text
        
        volume = volume.split('.')[-1][1:] 
        volume = volume.split(' ')[-1] 

        for section in sections[1:]:
            children = section.getchildren()
            title = children[0].getchildren()[0].text
            title = title.replace('.', '')

            if 'ЭПИЛОГ' in title:
                volume = 'ЭПИЛОГ'
            elif 'ЧАСТЬ' in title:
                part = title.split()[2]
                part = self.word2number[part]
            else:
                chapter = title
                chapter = chapter.replace('Х', 'X')
                if chapter != 'Примечания':
                    chapter = roman.fromRoman(chapter)

                lines = []
                for line in children:
                    if line.tag == f'{extra_str}p' and line.text is not None:
                        lines.append(line.text)

                self.create_doc(volume=str(volume), part=str(part), chapter=str(chapter),
                           name=name, author=author, text=' '.join(lines))

        return f"created {book_path}"


    def get_chapter(self, volume: int or str, part: int or str,
                    chapter: int or str, limit: None or int=None) -> str:
        '''
        Returns limited text of a certain chapter.

            Parameters:
                volume: Number of volume
                part: Number of part
                chapter: Number of chapter
                limit: Max number of symbols in returned text

            Returns:
                str: Text of chapter

        '''
        query = Q("match", volume=volume) & Q("match", part=part) & Q("match", chapter=chapter)
        search = self.BookDocument.search(index=self.index_name)
        search = search.query(query)
        response = search.execute()
        return response[0].text[:limit]

    def get_text(self, phrase: str) -> list:
        '''
        Returns a list of str "volume-part-chapter" where the phrase meets.

            Parameters:
                phrase: Search phrase

            Returns:
                list: List of str

        '''
        query = Q("match_phrase", text=phrase)
        search = self.BookDocument.search(index=self.index_name)
        search = search.query(query)
        response = search.execute()
        answer = [f'{res.volume}-{res.part}-{res.chapter}' for res in response]
        return answer

    def summarize_text(self, text: str) -> str:
        '''
        Returns an extractive summarized text.

            Parameters:
                text: The text for summarization

            Returns:
                str: A summarized text

        '''
        doc = self.nlp(text)

        # Calculate word frequencies from the text after removing stopwords and punctuations
        word_frequencies = {}
        for word in doc:
            if word.text.lower() not in self.stopwords and \
            word.text.lower() not in self.punctuation:
                if word.text not in word_frequencies:
                    word_frequencies[word.text] = 1
                else:
                    word_frequencies[word.text] += 1

        # Calculate the maximum frequency and divide it by all frequencies to get normalized
        # word frequencies
        max_frequency = max(word_frequencies.values())
        for word in word_frequencies:
            word_frequencies[word] = word_frequencies[word] / max_frequency

        # Get sentence tokens
        sentence_tokens = list(doc.sents)

        # Calculate the most important sentences by adding the word frequencies in each sentence
        sentence_scores = {}
        for sent in sentence_tokens:
            for word in sent:
                if word.text.lower() in word_frequencies:
                    if sent not in sentence_scores:
                        sentence_scores[sent] = word_frequencies[word.text.lower()]
                    else:
                        sentence_scores[sent] += word_frequencies[word.text.lower()]

        # Calculate 30% of text with maximum score
        select_length = int(len(sentence_tokens) * 0.3)
        summary = nlargest(select_length, sentence_scores,key=sentence_scores.get)

        # Get the summary of text
        final_summary = [word.text for word in summary]
        summary = ' '.join(final_summary)

        return summary
