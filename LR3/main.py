# pylint: disable=C0103,R1720

'''
Module for working with ElasticSearch throuhgt the console

'''

import argparse
import sys

import lab3

if __name__ == "__main__":
    elastic = lab3.Elastic('2018-3-4-vor', hosts=['172.17.0.1'])

    parser = argparse.ArgumentParser()

    parser.add_argument("act", choices=['create', 'add-fb2', 'get-chapter',
                                        'get-text', 'summarize-text'])
    parser.add_argument("-a", "--author", dest="author", type=str, default=None, required=False)
    parser.add_argument("-n", "--name", dest="name", type=str, default=None, required=False)
    parser.add_argument("-l", "--limit", dest="limit", type=str, default=None, required=False)
    parser.add_argument("-t", "--text", dest="text", type=str, default=None, required=False)
    parser.add_argument("-c", "--chapter", dest="chapter", type=str, default=None, required=False)

    action = sys.argv[1]

    if action == 'create':
        print(elastic.create_index())

    elif action == 'add-fb2':
        parser.add_argument('path')
        args = parser.parse_args()

        if not args.path:
            raise Exception(f"Wrong argument 'path={args.path}'") from None
        elif not args.name:
            raise Exception(f"Wrong argument 'name={args.name}'") from None
        elif not args.author:
            raise Exception(f"Wrong argument 'author={args.author}'") from None

        try:
            print(elastic.parse_book(args.path, args.name, args.author))
        except FileNotFoundError:
            raise Exception(f"Wrong book path '{args.book}'") from None

    elif action == 'get-chapter':
        parser.add_argument('phrase')
        args = parser.parse_args()

        if not args.phrase:
            raise Exception(f"Wrong argument 'phrase={args.phrase}'") from None

        volume, part, chapter = args.phrase.split('-')

        if args.limit is None:
            print(elastic.get_chapter(volume, part, chapter))

        try:
            if args.limit is None:
                limit = 0
            else:
                limit = int(args.limit)
            print(elastic.get_chapter(volume, part, chapter, limit))
        except ValueError:
            raise Exception(f"Wrong argument 'limit={args.limit}'") from None

    elif action == 'get-text':
        parser.add_argument('phrase')
        args = parser.parse_args()

        if not args.phrase:
            raise Exception(f"Wrong argument 'phrase={args.phrase}'") from None

        print(elastic.get_text(args.phrase))

    elif action == 'summarize-text':
        args = parser.parse_args()

        if not args.text and not args.chapter:
            raise Exception('There are not arguments text or chapter')

        if args.text:
            chapter = elastic.get_text(args.text)[0]
            text = elastic.get_chapter(*chapter.split('-'))
        elif args.chapter:
            chapter = args.chapter
            text = elastic.get_chapter(*chapter.split('-'))

        summary = elastic.summarize_text(text)
        print(elastic.summarize_text(summary))
