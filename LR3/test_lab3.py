'''
Module for testing class Elastic

'''

import time

from elasticsearch_dsl import Index

import lab3

elastic = lab3.Elastic('test_index')

def test_create_index():
    '''
    Tests the create_index function
    '''

    my_index = Index(elastic.index_name)
    if my_index.exists():
        my_index.delete()

    elastic.create_index()
    assert my_index.exists()
    mapping = my_index.get_mapping()[elastic.index_name]['mappings']['properties']
    assert mapping['name']['type'] == 'text'
    assert mapping['author']['type'] == 'text'
    assert mapping['text']['type'] == 'text'
    assert mapping['volume']['type'] == 'text'
    assert mapping['part']['type'] == 'text'
    assert mapping['chapter']['type'] == 'text'

def test_create_doc():
    '''
    Tests the create_doc function
    '''

    doc_id = 'test_id'
    if elastic.BookDocument.exists(id=doc_id, index=elastic.index_name):
        elastic.BookDocument(_id=doc_id).delete()

    elastic.create_doc(_id=doc_id, author='Tolstoy', name='ВойнаИМир', text='test test test')

    assert elastic.BookDocument.exists(doc_id, index=elastic.index_name)

def test_parse_book():
    '''
    Tests the parse_book function
    '''

    time.sleep(1)
    search = elastic.BookDocument.search(index=elastic.index_name)
    search.execute()
    assert search.count() == 1

    elastic.parse_book('./wap/Война и мир - Толстой - том 1.fb2', 'Война и мир', 'Толстой')

    time.sleep(2)
    search = elastic.BookDocument.search(index=elastic.index_name)
    search.execute()
    assert search.count() == 70

def test_get_chapter():
    '''
    Tests the get_chapter function
    '''

    volume = 1
    part = 2
    chapter = 3
    limit = 47

    response = elastic.get_chapter(volume, part, chapter, limit)
    assert response == 'Возвратившись со смотра, Кутузов, сопутствуемый'

def test_get_text():
    '''
    Tests the get_text function
    '''

    phrase = 'Возвратившись со смотра'
    response = elastic.get_text(phrase)
    assert ['1-2-3'] == response
